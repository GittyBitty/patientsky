﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace PatientSkyTest
{
    
    static class RegExPattern
    {
        // the main regex - used to build the tree
        internal readonly static string pattern_key_value_ref = @"(?ix)
                                    (?<data>
                                            (?<name> name:[^()|]*)|
                                            (?<ref> ref:[^()|]*)|
                                            (?<value> value:[^()|]*)|
                                            (?<open> \()|
                                            (?<close> \))
                                    )";

        // this regular expression is used for validating the start "#" and end symbol "&" (or tokens)
        // in the input file.
        internal readonly static string pattern_start_end = @"(?x) 
                                            ^\#  
                                            ([^()]|\(|\))* 
                                            &$";

        // this regular expression used to extract the individuals data elements from the input file
        internal readonly static string pattern_dataObject = @"(?x)
                                                (?<dataObject>
                                                 \(  
                                                     (   
                                                        (?<data>[^()&])   | (?<open>\()   |    (?<-open>\))    
                                                    )+
                                                \)
                                                )
                                        (?(open)(?!)) 
                                        ";
    }

    class DataPreProcessor
    {
        string fileName;
        string inputString;
        List<string> dataElems = new List<string>();

        public List<string> DataElems { get => dataElems; set => dataElems = value; }

        public DataPreProcessor(string fileName)
        {
            this.fileName = fileName;
            PrepareInput();
            ExtractDataElemsFromInputString();
        }
        private void PrepareInput()
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileName);

            if (File.Exists(path))
            {
                inputString = File.ReadAllText(path);

                inputString = inputString.Replace("\r\n", string.Empty);
            }
            else
                throw new Exception("Input data file not found !!");
        }

        private void ExtractDataElemsFromInputString()
        {
            try
            {
                Match start_end_match = Regex.Match(inputString, RegExPattern.pattern_start_end);

                if (start_end_match.Success)
                {
                    Match match = Regex.Match(inputString, RegExPattern.pattern_dataObject);

                    if (!match.Success)
                    {
                        throw new Exception("Syntax error in input file !!");
                    }

                    while (match.Success)
                    {
                        var tree = match.Groups["dataObject"].Value;
                        DataElems.Add(tree);
                        //Console.WriteLine(match.Value);
                        match = match.NextMatch();
                    }

                }
                else
                    throw new Exception("Syntax error in input file !!");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    class DataDeserializer
    {
        DataPreProcessor fileHandler;

        public NTree<Element> Root { get; set; } 

        public DataDeserializer(DataPreProcessor fileHandler)
        {
            this.fileHandler = fileHandler;
            Root = new NTree<Element>(new Element());
            BuildTree();
        }


        /// <summary>
        /// The main logic to build the tree data structure is here.
        /// It uses the regular expression to extract differernt key/value/ref and take care
        /// of opening and closing brackets.
        /// </summary>
        private void BuildTree()
        {
            var dataElems = fileHandler.DataElems;
            Element elem = Root.Data;

            foreach (string tree in dataElems)
            {

                Match match = Regex.Match(tree, RegExPattern.pattern_key_value_ref);

                NTree<Element> current = Root;

                elem = current.Data;

                string name = string.Empty,
                       value = string.Empty,
                       refer = string.Empty;

                while (match.Success)
                {
                    if (match.Groups["value"].Value.Length != 0)
                    {
                        value = match.Groups["value"].Value;
                        elem.Value = value;
                    }
                    else if (match.Groups["name"].Value.Length != 0)
                    {
                        name = match.Groups["name"].Value;
                        elem.Name = name;
                    }
                    else if (match.Groups["ref"].Value.Length != 0)
                    {
                        refer = match.Groups["ref"].Value;
                        elem.Refer = refer;
                    }
                    else if (match.Groups["open"].Value.Length != 0)
                    {
                        elem.ChildCount++;
                        current.Data = elem;

                        var newElem = new Element();
                        newElem.NodeId = current.Data.NodeId + " . " + current.Data.ChildCount.ToString();
                        newElem.Level = current.Data.Level+1;

                        current = current.AddChild(newElem);
                        elem = current.Data;
                    }
                    else if (match.Groups["close"].Value.Length != 0)
                    {
                        current.Data = elem;
                        current = current.Parent;
                        elem = current.Data;
                    }

                    match = match.NextMatch();
                }
            }
        }
    }
}
