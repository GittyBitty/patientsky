﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Schema;

// Save this file as Program.cs to a folder
// Add the data file containing the input data to the same folder
// Compile with: csc Program.cs to get the executable
// Run the program with data filename as commandline argument

namespace PatientSkyTest
{
    public class Program
    {
        static string treeOut = "";

        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                System.Console.WriteLine(" No Input filename entered !! ");
                return;
            }

            var fileHandler = new DataPreProcessor(args[0]);
            var dataDeserialzer = new DataDeserializer(fileHandler);

            //Console.ReadLine();
            NTree<Element> root = dataDeserialzer.Root;

            Console.WriteLine("\n\nDeserializer output (loaded data objects from the input file in to the tree data structure) : \n\n");

            // using TreeVisitor delegate for printing to the console
            root.Traverse(root, Program.PrintToConsole, null);

            // using TreeVisitor delegate for serializing to string
 
            root.Traverse(root, Program.SerializePre,  Program.SerializePost);

            // output treeOut string to file
            WriteToFile("stringOutput.txt");

            Console.ReadLine();
        }

        static void PrintToConsole(Element elem)
        {
            Console.WriteLine("{0}  ({1}) \n\t {2} {3} {4}", elem.NodeId, elem.Level, elem.Name, elem.Value, elem.Refer);
        }

        static void SerializePre(Element elem)
        {
            if ( elem.Level == 0)
                treeOut = treeOut + "#";
            if (elem.Level > 0)
            {
                treeOut = treeOut + "(" + elem.Name + "| " + elem.Value + "| " + elem.Refer;
            }              
        }

        static void SerializePost(Element elem)
        {
            if(elem.Level > 0)
                treeOut = treeOut + ")";
            if (elem.Level == 0)
                treeOut = treeOut + "&";
        }

        public static void WriteToFile(string filename)
        {
            File.WriteAllText(filename, treeOut);       
        }
    }
}

