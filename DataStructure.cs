﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace PatientSkyTest
{

    class Element
    {
        string value;
        string name;
        string refer;

        string nodeId;
        int level; 

        int childCount;

        public Element()
        {
            NodeId = "0";
            Value = string.Empty;
            Name = string.Empty;
            Refer = string.Empty;
            ChildCount = 0;
            Level = 0;
        }

        public string Value { get => value; set => this.value = value; }
        public string Name { get => name; set => name = value; }
        public string Refer { get => refer; set => refer = value; }
        public string NodeId { get => nodeId; set => nodeId = value; }
        public int Level { get => level; set => level = value; }
        public int ChildCount { get => childCount; set => childCount = value; }
    }

    delegate void TreeVisitor<T>(T nodeData);

    class NTree<T>
    {
        private LinkedList<NTree<T>> children;

        public NTree<T> Parent { get; set; }

        public T Data { get; set; }

        public NTree(T data)
        {
            Data = data;
            children = new LinkedList<NTree<T>>();
        }

        public NTree()
        {
            this.Data = default;
            children = new LinkedList<NTree<T>>();
        }

        public NTree<T> AddChild(T data)
        {
            var node = new NTree<T>(data);
            node.Parent = this;
            return children.AddLast(node).Value;
        }

        public NTree<T> GetChild(int i)
        {
            foreach (NTree<T> n in children)
                if (--i == 0)
                    return n;
            return null;
        }

        public void Traverse(NTree<T> node, TreeVisitor<T> visitorPreWork, TreeVisitor<T> visitorPostWork = null)
        {
            visitorPreWork(node.Data);
            foreach (NTree<T> kid in node.children)
                Traverse(kid, visitorPreWork, visitorPostWork);
            if(visitorPostWork!=null)
                 visitorPostWork(node.Data);
        }
    }
}
